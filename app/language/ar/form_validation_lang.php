<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'بيانات {field} إجباري.';
$lang['form_validation_isset']			= 'بيانات {field} يجب أن يحتوى على قيمة.';
$lang['form_validation_valid_email']		= 'بيانات {field} يجب أن يحتوى عنوان بريد إلكتروني صحيح.';
$lang['form_validation_valid_emails']		= 'بيانات {field} يجب أن يحتوى على عناوين بريد إلكتروني صحيحة.';
$lang['form_validation_valid_url']		= 'بيانات {field} يجب أن يحتوى على رابط.';
$lang['form_validation_valid_ip']		= 'بيانات {field} يجب أن يحتوى على عنوان أي بي.';
$lang['form_validation_min_length']		= 'بيانات {field} يجب أن لا يقل عن {param} حرف.';
$lang['form_validation_max_length']		= 'بيانات {field} يجب أن لا يتجاوز أكثر من {param} حرف.';
$lang['form_validation_exact_length']		= 'بيانات {field} يجب أن يكون بطول {param} حرف.';
$lang['form_validation_alpha']			= 'بيانات {field} يمكن أن يحتوى على أحرف فقط.';
$lang['form_validation_alpha_numeric']		= 'بيانات {field} يمكن أن يحتوى على أحرف وأرقام فقط.';
$lang['form_validation_alpha_numeric_spaces']	= 'بيانات {field} يمكن أن يحتوى على أحرف وأرقام و فراغات فقط.';
$lang['form_validation_alpha_dash']		= 'بيانات {field} يمكن أن يحتوى على أحرف وأرقام أو شرطة أو شرطة سفلية.';
$lang['form_validation_numeric']		= 'بيانات {field} يجب أن يحتوى على أرقام فقط.';
$lang['form_validation_is_numeric']		= 'بيانات {field} يجب أن يحتوى على أرقام فقط.';
$lang['form_validation_integer']		= 'بيانات {field} يجب أن يحتوى على رقم صحيح فقط.';
$lang['form_validation_regex_match']		= 'بيانات {field} لا يحتوى على قيمة بشكل صحيح.';
$lang['form_validation_matches']		= 'بيانات {field} لا يساوي بيانات {param}.';
$lang['form_validation_differs']		= 'بيانات {field} يجب أن يكون مختلف عن بيانات {param}.';
$lang['form_validation_is_unique'] 		= 'بيانات {field} يجب أن يحتوى على قيمة غير متكررة.';
$lang['form_validation_is_natural']		= 'بيانات {field} يجب أن يحتوى على رقم.';
$lang['form_validation_is_natural_no_zero']	= 'بيانات {field} يجب أن يحتوى على رقم أكبر من صفر.';
$lang['form_validation_decimal']		= 'بيانات {field} يجب أن يحتوى على رقم عشري.';
$lang['form_validation_less_than']		= 'بيانات {field} يجب أن يحتوى على رقم أقل من {param}.';
$lang['form_validation_less_than_equal_to']	= 'بيانات {field} يجب أن يحتوى على رقم أقل أو يساوي {param}.';
$lang['form_validation_greater_than']		= 'بيانات {field} يجب أن يحتوى على رقم أكبر من {param}.';
$lang['form_validation_greater_than_equal_to']	= 'بيانات {field} يجب أن يحتوى على رقم أكبر من أو يساوي {param}.';
$lang['form_validation_error_message_not_set']	= ' بيانات{field}  غير قادر على الوصول إلى رسالة خطأ المقابلة ل اسم مجال عملك.';
$lang['form_validation_in_list']		= 'The {field} field must be one of: {param}.'; //FIXME