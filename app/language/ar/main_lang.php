<?php
$lang['aply_email'] = 'البريد الإلكترونى';
$lang['aply_name'] = 'الإسم';
$lang['aply_phone'] = 'رقم التليفون';
$lang['msg'] = 'الرساله';
$lang['uploadCvError'] = 'من فضلك قم بإرفاق السيره الذاتيه الخاصه بك';
$lang['applySuccess'] = 'شكرا لك , سوف نقوم بلإتصال بك قريبا';
$lang['msgSuccess'] = 'تم استقبال الرساله , سنقوم بفحصها و الرد عليكم فى اقرب وقت';
$lang['birthDate'] = 'تاريخ ميلاد الطالب';
$lang['school_grade'] = 'المرحله التعليميه للطالب';
$lang['captcha'] = 'التحقق الامنى';
