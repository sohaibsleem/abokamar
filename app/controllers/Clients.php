<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Clients extends CI_Controller {
    public $user;
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            $this->ion_auth->logout();
            return redirect('log');
        }
        $this->load->model('clients_model','cli');
        $this->load->helper('main');
        $this->user = $this->ion_auth->user()->row()->id;
    }

    function index() {
        $this->all();
    }
    
    function all(){
        $config['base_url'] = site_url("Clients/all");
        $config['total_rows'] = $this->db->get('clients')->num_rows();
        $config['per_page'] = $limit = 20;
        $config['uri_segment'] = 3;
        include_once APPPATH . 'third_party/pagination.php';
        $offset = $this->uri->segment(3, 0);
        $data['clients'] = $this->cli->getClients($limit,$offset);
        $this->load->view('clients/all', $data);
    }
    
    
    function add(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('cl_name', 'اسم العميل', 'trim|required|is_unique[clients.cl_name]');
            if ($this->form_validation->run() == FALSE) {
                $data['done'] = 0;
                $data['msg'] = error_msg(validation_errors());
                goto ret;
            } else {
                if ($this->cli->add()) {
                   $data['done'] = 1;
                } else {
                     $data['done'] = 0;
                     $data['msg'] = error_msg("من فضلك قم بتحديث الصفحه و كرر المحاوله");
                     goto ret;
                }
            }
        ret:
         echo json_encode($data);
    }
    
    
       function getClientData($id){
           $data['content'] = $this->cli->getDetails($id);
           $this->load->view('clients/update', $data);
       }
    
       function update() {
        $this->load->library('form_validation');
        if(md5($this->input->post('cl_name')) === md5($this->input->post('currName'))){
           $this->form_validation->set_rules('cl_name', 'اسم العميل', 'trim|required'); 
        }else{
            $this->form_validation->set_rules('cl_name', 'اسم العميل', 'trim|required|is_unique[clients.cl_name]');
        }
         
            if ($this->form_validation->run() == FALSE) {
                $data['done'] = 0;
                $data['msg'] = error_msg(validation_errors());
           
            } else {
                if ($this->cli->update()) {
                   $data['done'] = 1;
                } else {
                     $data['done'] = 0;
                     $data['msg'] = error_msg("من فضلك قم بتحديث الصفحه و كرر المحاوله");
                }
            }

      
         echo json_encode($data);
    }
    
     function deleteCli() {
//          if ($this->itm->checkValidDeleteItm()) {
//            $data['done'] = 0;
//            $data['msg'] = "لا يمكن حذف التصنيف لارتباطه بفواتير للبيع";
//            goto ret;
//        }
        if ($this->cli->deleteCli()) {
            $data['done'] = 1;
            $data['msg'] = "تم حذف العميل بنجاح";
        } else {
            $data['done'] = 0;
            $data['msg'] = "من فضلك اعد المحاوله";
        }
        echo json_encode($data);
    }
    
    function bar(){
      
     $this->load->library('barcode');
    // echo $this->barcode->generate();
    }
    

}