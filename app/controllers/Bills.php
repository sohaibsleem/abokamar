<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Bills extends CI_Controller {
    public $user;
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            $this->ion_auth->logout();
            return redirect('log');
        }
        $this->load->model('bills_model','bil');
        $this->load->helper('main');
        $this->user = $this->ion_auth->user()->row()->id;
    }

    function index() {
        $this->all();
    }
    
    function all(){
        $config['base_url'] = site_url("Items/all");
        $config['total_rows'] = $this->db->get('items')->num_rows();
        $config['per_page'] = $limit = 20;
        $config['uri_segment'] = 3;
        include_once APPPATH . 'third_party/pagination.php';
        $offset = $this->uri->segment(3, 0);
        $data['categories'] = $this->itm->getCategory();
        $data['items'] = $this->itm->getItems($limit,$offset);
        $this->load->view('items/all', $data);
    }
    
    
    function recipt(){
    $data['clients'] = $this->bil->getClients();    
    $data['items'] = $this->bil->getItems(); 
    $this->load->view('bills/recipt', $data);
    }
    
   function items_prices($itm,$order){
    $data['prices'] = $this->bil->getItmPrices($itm); 
    $data['or'] = $order; 
    $this->load->view('bills/prices', $data);   
   }
   
   function billItems($or){
   $data['order'] = $or;  
   $data['items'] = $this->bil->getItems(); 
   $this->load->view('bills/bill_itms', $data);
   }
   
   function isBan(){
      if($this->bil->is_ban()->cl_ban == 1){
          $data['done'] = 0;
          $data['msg'] = "هذا العميل محظور التعامل معه";
          goto ret;
        } 
        
      if($this->bil->is_ban()->cl_trans_type == 1 && $this->input->post('ty') == 2){
          $data['done'] = 0;
          $data['msg'] = "غير مسموح بالتعامل الاجل مع العميل";
          goto ret;
        }  
      $data['done'] = 1;
      ret:
      echo json_encode($data);
   }
   
   function addBill(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('bils_for', 'العميل', 'greater_than[0]',['greater_than'=>'بيانات العميل مطلوبه']);
             foreach($this->input->post('items') as $i=>$itm){
             $this->form_validation->set_rules("items[$i][b_item]", 'العنصر', 'greater_than[0]',['greater_than'=>'يجب اختيار الصنف']);
            $this->form_validation->set_rules("items[$i][b_num]", 'عدد الوحدات', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules("items[$i][b_price]", 'السعر', 'trim|required|greater_than[0]'); 
             }
             

            if ($this->form_validation->run() == FALSE) {
                $data['done'] = 0;
                $data['msg'] = error_msg(validation_errors());
                goto ret;
            } else {
                
                $lstID = $this->bil->addBill();
                if ($lstID) {
                  $data['done'] = 1;  
                  $data['billId'] = $lstID;
                  
                  
                  
                } else {
                   $data['done'] = 0;
                   $data['msg'] = error_msg("من فضلك قم بتحديث الصفحه و كرر المحاوله");
                }
            }
      

        ret:
        echo json_encode($data);  
   }
   
    function bar($text){
     $this->load->library('barcode');
     $this->barcode->generate($text);
    }
    
   function billDetails($id){
       $data = [];
       $content = $this->bil->getBillDetails($id);
       foreach($content as $i=>$cont){
           if($i == 0){
            $data['bils_id'] = $cont->bils_id;   
            $data['bils_type'] = $cont->bils_type;   
            $data['first_name'] = $cont->first_name;   
            $data['created_on'] = $cont->created_on;   
            $data['cl_name'] = $cont->cl_name;   
            $data['bils_total'] = $cont->bils_total;   
            $data['bils_cash'] = $cont->bils_cash;   
            $data['bils_remain'] = $cont->bils_remain;   
           }
           $data['itms'][$i]['b_price'] = $cont->b_price; 
           $data['itms'][$i]['b_num'] = $cont->b_num; 
           $data['itms'][$i]['b_sum'] = $cont->b_sum; 
           $data['itms'][$i]['itm_title'] = $cont->itm_title; 
       }
      $this->load->view('bills/bill', $data);
   }
    
     function delete() {
//          if ($this->itm->checkValidDeleteItm()) {
//            $data['done'] = 0;
//            $data['msg'] = "لا يمكن حذف التصنيف لارتباطه بفواتير للبيع";
//            goto ret;
//        }
        if ($this->itm->deleteItm()) {
            $data['done'] = 1;
            $data['msg'] = "تم حذف العنصر بنجاح";
        } else {
            $data['done'] = 0;
            $data['msg'] = "من فضلك اعد المحاوله";
        }
        echo json_encode($data);
    }
    

    
    #____________categories___________
    
    function updateTable(){
       $data['categories'] = $this->itm->getCategory(); 
       $this->load->view('items/cateTlist', $data);
    }
    
    
    function addCat() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cat_title', 'اسم التصنيف', 'trim|is_unique[items_category.cat_title]|required');
        if ($this->form_validation->run() == FALSE) {
            $data['done'] = 0;
            $data['msg'] = error_msg(validation_errors());
        } else {
            if ($this->itm->addCat()) {
                $data['done'] = 1;
            } else {
                $data['done'] = 0;
                $data['msg'] = error_msg("من فضلك اعد المحاوله");
            }
        }
        ret:
        echo json_encode($data);
    }
    
    function edit_cat() {
        $this->load->library('form_validation');
        if($this->input->post('cur_title') != $this->input->post('cat_title')){ 
          $this->form_validation->set_rules('cat_title', 'اسم التصنيف', 'trim|is_unique[items_category.cat_title]|required');  
        }else{
         $this->form_validation->set_rules('cat_title', 'اسم التصنيف', 'trim');     
        }
        
        if ($this->form_validation->run() == FALSE) {
            $data['done'] = 0;
            $data['msg'] = validation_errors();
        } else {
            if ($this->itm->editCat()) {
                $data['done'] = 1;
                $data['msg'] = "تم تحديث اسم التصنيف";
            } else {
                $data['done'] = 0;
                $data['msg'] = "من فضلك اعد المحاوله";
            }
        }
        ret:
        echo json_encode($data);
    }
    
    
        function delete_cat() {
        if ($this->itm->checkValidDeleteCat()) {
            $data['done'] = 0;
            $data['msg'] = "لا يمكن حذف التصنيف لارتباطه بعناصر بيع";
            goto ret;
        }
        if ($this->itm->deleteCat()) {
            $data['done'] = 1;
            $data['msg'] = "تم حذف التصنيف";
        } else {
            $data['done'] = 0;
            $data['msg'] = "من فضلك اعد المحاوله";
        }
        ret:
        echo json_encode($data);
    }
    
    //prices
    
      function getItemPrices($id){
        $data['prices'] = $this->itm->getItemPrices($id);
        $this->load->view('items/prices', $data);   
    }
    
    
    function addPrice(){
       $this->load->library('form_validation');
        $this->form_validation->set_rules('pric_purchase', 'سعر الشراء', 'trim|numeric|required');
        $this->form_validation->set_rules('pric_stock', 'سعر المخزن', 'trim|numeric|required');
        $this->form_validation->set_rules('pric_wholesale', 'سعر الجملة', 'trim|numeric|required');
        $this->form_validation->set_rules('stock', 'الكميه المتاحه', 'trim|numeric|required');
        $this->form_validation->set_rules('unit', 'وحده البيع', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $data['done'] = 0;
            $data['msg'] = error_msg(validation_errors());
        } else {
            if ($this->itm->addPrice()) {
                $data['done'] = 1;
            } else {
                $data['done'] = 0;
                $data['msg'] = error_msg("من فضلك اعد المحاوله");
            }
        }
        ret:
        echo json_encode($data);   
    }

}