<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Items extends CI_Controller {
    public $user;
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            $this->ion_auth->logout();
            return redirect('log');
        }
        $this->load->model('items_model','itm');
        $this->load->helper('main');
        $this->user = $this->ion_auth->user()->row()->id;
    }

    function index() {
        $this->all();
    }
    
    function all(){
        $config['base_url'] = site_url("Items/all");
        $config['total_rows'] = $this->db->get('items')->num_rows();
        $config['per_page'] = $limit = 20;
        $config['uri_segment'] = 3;
        include_once APPPATH . 'third_party/pagination.php';
        $offset = $this->uri->segment(3, 0);
        $data['categories'] = $this->itm->getCategory();
        $data['items'] = $this->itm->getItems($limit,$offset);
        $this->load->view('items/all', $data);
    }
    
  
    
    function add(){
           $this->load->library('form_validation');
        if ($this->input->post('sub')) {
            $this->form_validation->set_rules('itm_title', 'اسم العنصر', 'trim|required');
            $this->form_validation->set_rules('itm_cat', 'التصنيف','greater_than[0]|required');

            if ($this->form_validation->run() == FALSE) {
                goto ret;
            } else {
                if ($this->itm->addItems()) {
                    return redirect(site_url("items/all"));
                } else {
                    $this->session->set_flashdata('itm_Err', error_msg("من فضلك قم بتحديث الصفحه و كرر المحاوله"));
                }
            }
        }

        ret:
        $data['categories'] = $this->itm->getCategory();
        $this->load->view('items/add',$data);
    }
    
    
       function update($id) {
        $this->load->library('form_validation');
        if ($this->input->post('sub')) {
             $this->form_validation->set_rules('itm_title', 'اسم العنصر', 'trim|required');
            $this->form_validation->set_rules('itm_cat', 'التصنيف','greater_than[0]|required');

           if ($this->form_validation->run() == FALSE) {
                goto ret;
            } else {
                if ($this->itm->updateItems($id)) {
                    return redirect(site_url("items/all"));
                } else {
                    $this->session->set_flashdata('itm_Err', error_msg("من فضلك قم بتحديث الصفحه و كرر المحاوله"));
                }
            }
        }

        ret:   
        $data['content'] = $this->itm->getDetails($id);
        $data['categories'] = $this->itm->getCategory();
        $this->load->view('items/update',$data);
    }
    
     function delete() {
//          if ($this->itm->checkValidDeleteItm()) {
//            $data['done'] = 0;
//            $data['msg'] = "لا يمكن حذف التصنيف لارتباطه بفواتير للبيع";
//            goto ret;
//        }
        if ($this->itm->deleteItm()) {
            $data['done'] = 1;
            $data['msg'] = "تم حذف العنصر بنجاح";
        } else {
            $data['done'] = 0;
            $data['msg'] = "من فضلك اعد المحاوله";
        }
        echo json_encode($data);
    }
    

    
    #____________categories___________
    
    function updateTable(){
       $data['categories'] = $this->itm->getCategory(); 
       $this->load->view('items/cateTlist', $data);
    }
    
    
    function addCat() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cat_title', 'اسم التصنيف', 'trim|is_unique[items_category.cat_title]|required');
        if ($this->form_validation->run() == FALSE) {
            $data['done'] = 0;
            $data['msg'] = error_msg(validation_errors());
        } else {
            if ($this->itm->addCat()) {
                $data['done'] = 1;
            } else {
                $data['done'] = 0;
                $data['msg'] = error_msg("من فضلك اعد المحاوله");
            }
        }
        ret:
        echo json_encode($data);
    }
    
    function edit_cat() {
        $this->load->library('form_validation');
        if($this->input->post('cur_title') != $this->input->post('cat_title')){ 
          $this->form_validation->set_rules('cat_title', 'اسم التصنيف', 'trim|is_unique[items_category.cat_title]|required');  
        }else{
         $this->form_validation->set_rules('cat_title', 'اسم التصنيف', 'trim');     
        }
        
        if ($this->form_validation->run() == FALSE) {
            $data['done'] = 0;
            $data['msg'] = validation_errors();
        } else {
            if ($this->itm->editCat()) {
                $data['done'] = 1;
                $data['msg'] = "تم تحديث اسم التصنيف";
            } else {
                $data['done'] = 0;
                $data['msg'] = "من فضلك اعد المحاوله";
            }
        }
        ret:
        echo json_encode($data);
    }
    
    
        function delete_cat() {
        if ($this->itm->checkValidDeleteCat()) {
            $data['done'] = 0;
            $data['msg'] = "لا يمكن حذف التصنيف لارتباطه بعناصر بيع";
            goto ret;
        }
        if ($this->itm->deleteCat()) {
            $data['done'] = 1;
            $data['msg'] = "تم حذف التصنيف";
        } else {
            $data['done'] = 0;
            $data['msg'] = "من فضلك اعد المحاوله";
        }
        ret:
        echo json_encode($data);
    }
    
    //prices
    
      function getItemPrices($id){
        $data['prices'] = $this->itm->getItemPrices($id);
        $this->load->view('items/prices', $data);   
    }
    
    
    function addPrice(){
       $this->load->library('form_validation');
        $this->form_validation->set_rules('pric_purchase', 'سعر الشراء', 'trim|numeric|required');
        $this->form_validation->set_rules('pric_stock', 'سعر المخزن', 'trim|numeric|required');
        $this->form_validation->set_rules('pric_wholesale', 'سعر الجملة', 'trim|numeric|required');
        $this->form_validation->set_rules('stock', 'الكميه المتاحه', 'trim|numeric|required');
        $this->form_validation->set_rules('unit', 'وحده البيع', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $data['done'] = 0;
            $data['msg'] = error_msg(validation_errors());
        } else {
            if ($this->itm->addPrice()) {
                $data['done'] = 1;
            } else {
                $data['done'] = 0;
                $data['msg'] = error_msg("من فضلك اعد المحاوله");
            }
        }
        ret:
        echo json_encode($data);   
    }

}