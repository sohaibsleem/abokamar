<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('main');
        $this->load->library('form_validation');
    }

    function index() {
        
        $this->auth();
    }

    function auth() {
        if ($this->input->post('sub')) {

            if ($this->checkCsrf() == FALSE) {
                $error = "تم حظر الدخول لدواعى امنية, من فضلك حدث الصفحة و حاول مرة اخرى";
                $err = error_msg($error);
                $this->session->set_flashdata('login_err', $err);
                redirect(site_url("log"));
                return;
            }
            $this->load->helper('security');
            $this->form_validation->set_rules('email', 'البريد الإلكترونى', 'trim|required');
            $this->form_validation->set_rules('password', 'كلمة المرور', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                goto retu;
            } else {
                $email = $this->input->post('email', TRUE);
                $password = $this->input->post('password', TRUE);
                $remember = FALSE;
                if ($this->ion_auth->login($email, $password, $remember)) {
                  
                    redirect(site_url('home'));
                    return;
                } else {
                    $error = $this->ion_auth->errors();
                    $err = error_msg($error);
                    $this->session->set_flashdata('login_err', $err);
                    goto retu;
                }
            }
        } else {
            retu:
            $data['token'] = $this->getCsrf();
            $this->load->view('main/login', $data);
        }
    }
    
    function helpForgetPass(){
      $data['token'] = $this->getCsrf();
      $this->load->view('main/forget', $data);  
    }

    function generateCode() {
        if (!$this->checkCsrf()) {
            $err = error_msg("تم انتهاء صلاحيه الصفحه,من فضلك اعد تحميل الصفحه ثم كرر المحاوله");
            $this->session->set_flashdata('login_err', $err);
            goto terminate;
        }
        $this->load->helper('security');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'البريد الإلكترونى', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            goto terminate;
        }
        $email = $this->input->post('email');
        if ($this->ion_auth->forgotten_password($email)) {

            $msg = $this->ion_auth->messages();
            $err = success_msg($msg);
            $this->session->set_flashdata('login_err', $err);
        } else {
            $err = error_msg($this->ion_auth->errors());
            $this->session->set_flashdata('login_err', $err);
        }

            terminate:
            $data['token'] = $this->getCsrf();
            $this->load->view('main/login', $data);
    }

    function bindCodePass($code) {
        if ($code == "") {
            $error = "Invalid verification Code!!";
            $err = error_msg($error);
            $this->session->set_flashdata('login_err', $err);
            redirect(site_url("log"));
        }
        $reset = $this->ion_auth->forgotten_password_complete($code);
        if ($reset) {
            $msg ="يمكنك استخدام كلمه السر المرسله الى بريدكم الإلكترونى فى الدخول ";
            $msg .= $reset;
            $err = success_msg($msg);
            $this->session->set_flashdata('login_err', $err);
            redirect(site_url("log"));
        } else {
            $error = $this->ion_auth->errors();
            $err = error_msg($error);
            $this->session->set_flashdata('login_err', $err);
            redirect(site_url("log"));
        }
    }

    /*
     * security functions
     */

    protected function getCsrf() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    protected function checkCsrf() {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== NULL &&
                $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue') &&
                $this->input->post('chSpam') == "") {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function out() {
        $this->ion_auth->logout();
        redirect(site_url("log"));
    }

}
