<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public $user;

    function __construct() {

        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            $this->ion_auth->logout();
            return redirect('log');
        }
        $this->load->helper('main');
        $this->user = $this->ion_auth->user()->row()->id;
    }

    function index() {
        $this->load->view('main/home');
    }

}
