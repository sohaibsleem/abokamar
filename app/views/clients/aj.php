        <div ><!-- modal -->
            <div class="modal fade" id="cliModal" tabindex="-1" role="dialog" aria-labelledby="cliAdd" style="z-index:9999">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="cliAdd">  <i class="fa fa-plus" ></i>  اضافه عملاء</h4>
                        </div>
                        <div class="modal-body">
                            <div id="frm_err" ></div>
                            <form id="cliFrm" >
                                <div class="form-group">
                                    <label for="cl_name">الإسم</label>
                                    <input type="text" name="cl_name" class="form-control input-lg" id="cl_name">
                                </div>
                                <div class="form-group">
                                    <label for="cl_phone">ارقام التليفون</label>
                                    <input type="text" name="cl_phone" class="form-control input-lg" id="cl_phone">
                                </div>
                                <div class="form-group">
                                    <label for="cl_mobile">ارقام المحمول</label>
                                    <input type="text" name="cl_mobile" class="form-control input-lg" id="cl_mobile">
                                </div>
                                <div class="form-group">
                                    <label for="cl_trans_type">نوع المعاملات <span class="text-danger">*</span></label>
                                    <select id="cl_trans_type"  name="cl_trans_type" style="height: 37px;width: 100%" >
                                        <option value="1"> نقدى</option>
                                        <option value="2"> آجل </option>
                                        <option value="3"> الكل </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="cl_info">معلومات اضافيه</label>
                                    <textarea  name="cl_info" class="form-control" id="cl_info"></textarea>
                                </div>
                                <div class="form-group">
                                     <label for="cl_ban">حظر التعامل </label>
                                     <label id="cl_ban">
                                        <input name="cl_ban" class="checkbox-slider colored-darkorange" type="checkbox">
                                        <span class="text"></span>
                                    </label>
                                </div>
                                 <div class="form-group">
                                    <label for="cl_ban_resone">سبب الحظر</label>
                                    <textarea  name="cl_ban_resone" class="form-control" id="cl_ban_resone"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="plzAddCli">اضافه</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">غلق</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div ><!-- modal -->
            <div class="modal fade" id="cliModalE" tabindex="-1" role="dialog" aria-labelledby="cliAddE" style="z-index:9999">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="cliAddE">  <i class="fa fa-pencil-square" ></i>  تعديل بيانات العميل</h4>
                        </div>
                        <div class="modal-body">
                            <div id="frm_errE" ></div>
                            <form id="cliFrmE" >
                               
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="plzUpdateCli">تعديل</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">غلق</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
