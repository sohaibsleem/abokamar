<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('main/header'); ?>
    </head>
    <body class="main_font">
        <div class="navbar">
            <?php $this->load->view('main/upper_bar'); ?>  

        </div>
        <!-- Main Container -->
        <div class="main-container container-fluid">
            <!-- Page Container -->
            <div class="page-container">
                <!-- chat bar -->
                <?php $this->load->view('main/side_bar'); ?>    
                <!-- Page Content -->
                <div class="page-content">
                    <!-- Page Breadcrumb -->
                    <div class="page-breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= site_url('home') ?>">الرئيسيه</a>
                            </li>
                            <li class="active">العملاء</li>
                        </ul>
                    </div>
                    <!-- /Page Breadcrumb -->
                    <!-- Page Header -->
                    <div class="page-header position-relative">
                        <div class="header-title">
                            <h1>
                                العملاء
                            </h1>
                        </div>
                        <!--Header Buttons-->
                        <div class="header-buttons">
                            <a class="sidebar-toggler" href="#">
                                <i class="fa fa-arrows-h"></i>
                            </a>
                            <a class="refresh" id="refresh-toggler" href="">
                                <i class="glyphicon glyphicon-refresh"></i>
                            </a>
                            <a class="fullscreen" id="fullscreen-toggler" href="#">
                                <i class="glyphicon glyphicon-fullscreen"></i>
                            </a>
                        </div>
                        <!--Header Buttons End-->
                    </div>

                    <div class="page-body">
                        <h5 class="row-title">
                            <i class="typcn typcn-plus-outline"></i>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#cliModal">اضافه عميل </a> 
                        </h5> 
                        
                       
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="well with-header  with-footer">
                                    <div class="header bg-info">
                                      العملاء
                                    </div>
                                    <table class="table table-hover table-bordered">
                                        <thead class=" bordered-info">
                                            <tr>
                                                <th>
                                                    #
                                                </th>
                                                <th>
                                                    اسم العميل
                                                </th>
                                                 <th>
                                                    نوع المعاملات
                                                </th>
                                                <th>
                                                   بيانات الاتصال
                                                </th>
                                                 <th>
                                                    تاريخ الاضافه   
                                                </th>
                                                <th>
                                                  عميل محظور
                                                </th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($clients as $k => $cli): ?>
                                                <tr id="tr_<?= $cli->cl_id ?>">
                                                    <td>
                                                        <?= $k + 1 ?>
                                                        <div class="btn-group">
                                                            <a title="الاعدادات" data-toggle="dropdown">
                                                                <i class="glyphicon glyphicon-cog"></i>
                                                            </a>
                                                            <ul class="dropdown-menu">
                                                                <li>
                                                                    <a target="_blank" href=""><i class="fa fa-eye"></i>سجل العميل</a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li>
                                                                    <a target="_blank" href=""><i class="fa fa-file"></i>الفواتير</a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li>
                                                                    <a target="_blank" href=""><i class="fa fa-dollar"></i>حسابات اجله</a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li>
                                                                    <a href="javascript:void(0)" class="editCli" idd="<?= $cli->cl_id ?>"><i class="fa fa-pencil"></i> تعديل</a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li>
                                                                    <a href="javascript:void(0);" class="delCli" idd="<?= $cli->cl_id ?>"><i class="fa fa-trash-o"></i>حذف </a>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?= $cli-> cl_name  ?>                                     
                                                    </td>
                                                    <td>
                                                        <?= ($cli->cl_trans_type == 1)?'نقدى':($cli->cl_trans_type == 2)?'اجل':'نقدى + اجل' ?>                                     
                                                    </td>
                                                    <td>
                                                        <?= $cli->cl_phone ?>  <br/>
                                                        <?= $cli->cl_mobile ?>
                                                        
                                                    </td>
                                                    <td>
                                                        <?= date('d/m/Y H:i:s', $cli->created_on) ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?= ($cli->cl_ban)?"<i class='fa fa-check text-danger fa-2x' ></i>":"" ?>                                     
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>

                                    <div class="footer">
                                        <?= $this->pagination->create_links(); ?>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>  <!-- /Page body-->
                </div>
                <!--/ page content-->

            </div>
            <!-- /Page Container -->
            <!-- Main Container -->
        </div>
        <?php $this->load->view('clients/aj');   ?>

        
        
        <!--Basic Scripts-->
        <script src="<?= THEME; ?>js/jquery-2.0.3.min.js"></script>
        <script src="<?= THEME; ?>js/bootstrap.min.js"></script>
        <script src="<?= THEME; ?>js/slimscroll/jquery.slimscroll.min.js"></script>

        <!--Beyond Scripts-->
        <script src="<?= THEME; ?>js/skins.min.js"></script>
        <script src="<?= THEME; ?>js/beyond.min.js"></script>
        <script src="<?= THEME; ?>js/toastr/toastr.js"></script>
        <script>
            jQuery(document).ready(function () {
                //add category
                $("#plzAddCli").click(function () {
                    var data = $("#cliFrm").serialize();
                    var url = "<?= site_url('clients/add') ?>";
                    $.post(url, data, function (res) {
                        if (res.done == 1) {
                       location.reload();
                        } else {
                            $("#frm_err").html(res.msg);
                        }
                    }, 'json');


                });
                
                $("#plzUpdateCli").click(function () {
                    var data = $("#cliFrmE").serialize();
                    var url = "<?= site_url('clients/update') ?>";
                    $.post(url, data, function (resE) {
                        if (resE.done == 1) {
                          location.reload();
                        } else {
                            $("#frm_errE").html(resE.msg);
                        }
                    }, 'json');


                });
                $("#cliModal").on('hidden.bs.modal', function (e) {
                    $("#cliFrm")[0].reset();
                    $("#frm_err").html(' ');
                });

                //edit category
                   $(document).on('click','.editCli',function () {
                    $("#cliFrmE").load("<?= site_url('clients/getClientData') ?>/"+$(this).attr('idd'));
                    $("#cliModalE").modal();
                });
                
                 $("#cliModalE").on('hidden.bs.modal', function (e) {
                    $("#cliFrmE")[0].reset();
                    $("#frm_errE").html(' ');
                });


                $('.delCli').click(function () {
                    var conf = confirm('هل ترغب فى حذف العميل؟');
                    if (!conf) {
                        return false;
                    }
                    var idd = $(this).attr('idd');
                    var url = "<?= site_url('clients/deleteCli') ?>";
                    $.post(url, {id: idd}, function (res) {
                        if (res.done == 1) {
                            $('#tr_' + idd).remove();
                            Notify(res.msg, 'top-right', '5000', 'success', 'fa-check', true);

                        } else {
                            Notify(res.msg, 'top-right', '10000', 'danger', 'fa-times', true);
                        }
                    }, 'json');
                });

            });

        </script>
    </body>
    <!--  /Body -->
</html>
