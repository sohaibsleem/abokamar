<div class="form-group">
    <label for="cl_name">الإسم</label>
    <input type="text" value="<?=$content->cl_name?>" name="cl_name" class="form-control input-lg" id="cl_name">
    <input type="hidden" value="<?=$content->cl_name?>" name="currName" />
    <input type="hidden" value="<?=$content->cl_id?>" name="cl_id" />
</div>
<div class="form-group">
    <label for="cl_phone">ارقام التليفون</label>
    <input type="text" value="<?=$content->cl_phone?>" name="cl_phone" class="form-control input-lg" id="cl_phone">
</div>
<div class="form-group">
    <label for="cl_mobile">ارقام المحمول</label>
    <input type="text" value="<?=$content->cl_mobile?>" name="cl_mobile" class="form-control input-lg" id="cl_mobile">
</div>
<div class="form-group">
    <label for="cl_trans_type">نوع المعاملات <span class="text-danger">*</span></label>
    <select id="cl_trans_type"  name="cl_trans_type" style="height: 37px;width: 100%" >
        <option value="1" <?=($content->cl_trans_type == 1)?'selected':''?> > نقدى</option>
        <option value="2" <?=($content->cl_trans_type == 2)?'selected':''?>> آجل </option>
        <option value="3" <?=($content->cl_trans_type == 3)?'selected':''?>> الكل </option>
    </select>
</div>
<div class="form-group">
    <label for="cl_info">معلومات اضافيه</label>
    <textarea  name="cl_info" class="form-control" id="cl_info"><?=$content->cl_info?></textarea>
</div>
<div class="form-group">
    <label for="cl_ban">حظر التعامل </label>
    <label id="cl_ban">
        <input name="cl_ban" class="checkbox-slider colored-darkorange" <?=($content->cl_ban == 1)?'checked':''?> type="checkbox">
        <span class="text"></span>
    </label>
</div>
<div class="form-group">
    <label for="cl_ban_resone">سبب الحظر</label>
    <textarea  name="cl_ban_resone" class="form-control" id="cl_ban_resone"><?=$content->cl_ban_resone?></textarea>
</div>