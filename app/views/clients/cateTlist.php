<?php foreach ($categories as $ck => $cat): ?>
    <tr id="trCat_<?= $cat->cat_id ?>">
        <td>
            <?= $ck + 1 ?>
        </td>
        <td>
            <input type="text" id="catData_title_<?= $cat->cat_id ?>" value="<?= $cat->cat_title ?>" style="width: 100%;height: 30px;border: 0;border-bottom: 1px dashed grey;background-color: transparent;cursor: text"/>    
        </td>
        <td>
            <input type="text" id="catData_desc_<?= $cat->cat_id ?>" value="<?= $cat->cat_desc ?>" style="width: 100%;height: 30px;border: 0;border-bottom: 1px dashed grey;background-color: transparent;cursor: text;text-align: right"/>    
        </td>
        <td style=" text-align: center">
            <button class="btn btn-circle btn-xs btn-info editCat" data-cur_title="<?= $cat->cat_title ?>" data-id="<?= $cat->cat_id ?>" ><i class="fa fa-pencil"></i></button>
            <button class="btn btn-circle btn-xs btn-danger delCat"   data-id="<?= $cat->cat_id ?>"><i class="fa fa-trash-o"></i></button>                                        
        </td>
    </tr>
<?php endforeach; ?>           