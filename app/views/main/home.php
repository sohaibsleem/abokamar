<!DOCTYPE html>
<html>
    <!-- Head -->
    <head>

        <?php $this->load->view('main/header'); ?>
    </head>
    <!-- /Head -->
    <!-- Body -->
    <body class="main_font">
        <!-- Loading Container -->
        <div class="loading-container">
            <div class="loader"></div>
        </div>
        <!--  /Loading Container -->
        <!-- Navbar -->
        <div class="navbar">
            <?php $this->load->view('main/upper_bar'); ?>  

        </div>
        <!-- Main Container -->
        <div class="main-container container-fluid">
            <!-- Page Container -->
            <div class="page-container">
                <!-- chat bar -->
                <?php $this->load->view('main/side_bar'); ?>    
                <!-- Page Content -->
                <div class="page-content">
                    <!-- Page Breadcrumb -->
                    <div class="page-breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="#">الرئيسيه</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /Page Breadcrumb -->
                    <!-- Page Header -->
                    <div class="page-header position-relative">
                        <div class="header-title">
                            <h1>
                                <i class="fa fa-home" ></i>
                                الرئيسيه                           
                            </h1>
                        </div>
                        <!--Header Buttons-->
                        <div class="header-buttons">
                            <a class="sidebar-toggler" href="#">
                                <i class="fa fa-arrows-h"></i>
                            </a>
                            <a class="refresh" id="refresh-toggler" href="">
                                <i class="glyphicon glyphicon-refresh"></i>
                            </a>
                            <a class="fullscreen" id="fullscreen-toggler" href="#">
                                <i class="glyphicon glyphicon-fullscreen"></i>
                            </a>
                        </div>
                        <!--Header Buttons End-->
                    </div>
                    <!-- /Page Header -->
                    <!--Page body-->
                    <div class="page-body">
                     <?= $this->session->flashdata('autMsg'); ?>
                        <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">

                        </div>
                    </div>
                        
                    </div>  <!-- /Page body-->
                </div>
                <!--/ page content-->

            </div>
            <!-- /Page Container -->
            <!-- Main Container -->
        </div>

        <!--Basic Scripts-->
        <script src="<?= THEME; ?>js/jquery-2.0.3.min.js"></script>
        <script src="<?= THEME; ?>js/bootstrap.min.js"></script>
        <script src="<?= THEME; ?>js/slimscroll/jquery.slimscroll.min.js"></script>

        <!--Beyond Scripts-->
        <script src="<?= THEME; ?>js/skins.min.js"></script>
        <script src="<?= THEME; ?>js/beyond.min.js"></script>
    </body>
    <!--  /Body -->
</html>