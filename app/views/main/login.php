<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!--Head-->
<head>
    <meta charset="utf-8" />
    <title>Login Page</title>

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?= THEME . 'img/logo.jpg' ?>img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
     <!--Basic Styles-->
    <link href="<?= THEME;   ?>css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/bootstrap-rtl.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/weather-icons.min.css" rel="stylesheet" />

        <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/earlyaccess/droidarabickufi.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <!--Beyond styles-->
    <link href="<?= THEME;   ?>css/beyond-rtl.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/4095-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= THEME;   ?>css/demo.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/typicons.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/animate.min.css" rel="stylesheet" />
     <link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="<?= THEME;   ?>js/skins.min.js"></script>4
      <style>
        .main_font{
            font-family: 'Cairo', sans-serif !important;
        }
          .logo_font{
            font-family: 'Reem Kufi', sans-serif !important;  
        } 
        
     </style>
</head>
<!--Head Ends-->
<!--Body-->
<body class=" main_font">
    <div class="login-container animated fadeInDown">
        <div class="loginbox bg-white">
            <div class="loginbox-title main_font">تسجيل الدخول</div>
            <div class="loginbox-social">
                <div class="social-title ">من فضلك ادخل بيانات الدخول بشكل صحيح</div>
            </div>
            <div class="loginbox-or">
                <div class="or-line"></div>
            </div>
            <form method="POST" action="<?= site_url('log/auth')?>" >
                   <?=  $this->session->flashdata('login_err');?>
            <div class="loginbox-textbox">
                <input type="email" class="form-control" name="email" placeholder="البريد الإلكترونى" />
                <?= form_error('email', '<span class="help-block" style="color:red">', '</span>'); ?>
            </div>
            <div class="loginbox-textbox">
                <input type="password" class="form-control" name="password" placeholder="الرقم السرى" />
                <?= form_error('password', '<span class="help-block" style="color:red">', '</span>'); ?>
            </div>
            <div class="loginbox-forgot">
                <a href="<?= site_url('log/helpForgetPass')  ?>">هل نسيت بيانات الدخول؟</a>
            </div>
                 <?php foreach($token as $k=>$v):   ?>
                <input type="hidden" name="<?=$k?>" value="<?=$v?>" />
                <input type="hidden" name="chSpam" value="" />
                <?php endforeach;   ?>
            <div class="loginbox-submit">
                <input type="submit" name="sub" class="btn btn-primary btn-block" value="تسجيل الدخول">
            </div>
            </form>
        </div>
        <div class="logobox" style="height: 110px !important;">
          <h1 class="logo_font">مؤسسة سعد أبو قمر للتجارة</h1>
        </div>
    </div>

    <!--Basic Scripts-->
    <script src="<?= THEME;   ?>js/jquery-2.0.3.min.js"></script>
    <script src="<?= THEME;   ?>js/bootstrap.min.js"></script>
    <script src="<?= THEME;   ?>js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="<?= THEME;   ?>js/beyond.js"></script>

    
</body>
<!--Body Ends-->
</html>
