            <!-- Page Sidebar -->
            <div class="page-sidebar" id="sidebar" style="top:40px !important;">
                <ul class="nav sidebar-menu">
                    <!--Dashboard-->
                    <li>
                        <a href="<?= site_url('home')?>">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> الرئيسيه</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('items')?>">
                            <i class="menu-icon glyphicon glyphicon-list"></i>
                            <span class="menu-text"> الأصناف</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('clients')?>">
                            <i class="menu-icon glyphicon glyphicon-briefcase"></i>
                            <span class="menu-text"> العملاء</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('bills/recipt')?>">
                            <i class="menu-icon glyphicon glyphicon-file"></i>
                            <span class="menu-text"> فاتورة جديده</span>
                        </a>
                    </li>
                
               
                    <li>
                        <a href="mailto:sohaibelromy@gmail.com">
                            <i class="menu-icon glyphicon glyphicon-fire themesecondary"></i>
                            <span class="menu-text">
                             sohaibelromy@gmail.com
                            </span>
                        </a>
                    </li>
                </ul>
                <!-- /Sidebar Menu -->
            </div>
            <!-- /Page Sidebar -->
           