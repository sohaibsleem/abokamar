      <meta charset="utf-8" />
    <title>لوحه التحكم</title>    
    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?= THEME . 'img/attach-red.png' ?>" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<?= THEME;   ?>css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/bootstrap-rtl.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/earlyaccess/droidarabickufi.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Aref+Ruqaa" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Rakkas" rel="stylesheet">
    <!--Beyond styles-->
    <link href="<?= THEME;   ?>css/beyond-rtl.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/4095-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= THEME;   ?>css/demo.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/typicons.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/animate.min.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/introjs.css" rel="stylesheet" />
    <link href="<?= THEME;   ?>css/introjs-rtl.css" rel="stylesheet" />
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />
    <style>
        .main_font{
            font-family: 'Cairo', sans-serif !important;
        } 
        
        .logo_font{
            font-family: 'Reem Kufi', sans-serif !important;  
        } 
        
        .recipt_font{
            font-family: 'Aref Ruqaa', sans-serif !important;
        }
        
     </style>
     
     