<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('main/header'); ?>
    </head>
    <body class="main_font">
        <div class="navbar">
            <?php $this->load->view('main/upper_bar'); ?>  

        </div>
        <!-- Main Container -->
        <div class="main-container container-fluid">
            <!-- Page Container -->
            <div class="page-container">
                <!-- chat bar -->
                <?php $this->load->view('main/side_bar'); ?>    
                <!-- Page Content -->
                <div class="page-content">
                    <!-- Page Breadcrumb -->
                    <div class="page-breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= site_url('home') ?>">الرئيسيه</a>
                            </li>
                            <li>
                                <i class="fa fa-list"></i>
                                <a href="<?= site_url('bills') ?>">الفواتير</a>
                            </li>
                            <li class="active">فاتوره جديده</li>
                        </ul>
                    </div>
                    <!-- /Page Breadcrumb -->
                    <!-- Page Header -->
                    <div class="page-header position-relative">
                        <div class="header-title">
                            <h1>
                                فاتوره جديده
                            </h1>
                        </div>
                        <!--Header Buttons-->
                        <div class="header-buttons">
                            <a class="sidebar-toggler" href="#">
                                <i class="fa fa-arrows-h"></i>
                            </a>
                            <a class="refresh" id="refresh-toggler" href="">
                                <i class="glyphicon glyphicon-refresh"></i>
                            </a>
                            <a class="fullscreen" id="fullscreen-toggler" href="#">
                                <i class="glyphicon glyphicon-fullscreen"></i>
                            </a>
                        </div>
                        <!--Header Buttons End-->
                    </div>

                    <div class="page-body">
                        <h5 class="row-title"><i class="typcn typcn-th-list"></i><a href="<?= site_url('bills') ?>" >الفواتير</a></h5>
                        <div class="row">

                            <div class="col-lg-9 col-md-offset-1 col-sm-12 col-xs-12">
                                
                                <?= $this->session->flashdata('itm_Err'); ?>
                                <div class="widget">
                                    <div class="widget-header bordered-bottom bordered-themeprimary">
                                        <span class="widget-caption"><i class="typcn typcn-plus-outline"></i> فاتوره جديده</span>
                                    </div>
                                    <div class="widget-body bordered-left bordered-themeprimary">
                                        <div class="collapse in">
                                            <form id="bilFrm">
                                                <div id="billsError" ></div>
                                                <div class="form-group">
                                                    <label for="bils_type ">نوع الفاتوره <span class="text-danger">*</span> </label>
                                                    <select id="bils_type"  name="bils_type" style="height: 37px;width: 100%" >
                                                        <option value="1">نقدى</option>
                                                        <option value="2">اجل</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="bils_for">العميل <span class="text-danger">*</span> <span id="cl_case" class="text-danger" ></span></label>
                                                    <select id="bils_for"  name="bils_for" style="height: 37px;width: 100%" >
                                                        <option value="0"></option>
                                                        <?php foreach ($clients as $cli): ?>
                                                            <option value="<?= $cli->cl_id ?>" style="color:red" ><?= $cli->cl_name ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="table-responsive padding-bottom-30">
                                                    <table class="table table-hover table-bordered" >
                                                        <thead class=" bordered-info">
                                                            <tr>
                                                                <th style="width:1%">
                                                                    #
                                                                </th>
                                                                <th style="width:50%">
                                                                    الصنف
                                                                </th>
                                                                <th style="width:20%">
                                                                    كود السعر
                                                                </th>
                                                                <th style="width:9%">
                                                                    عدد
                                                                </th>
                                                                <th style="width:10%">
                                                                    سعر البيع
                                                                </th>
                                                                <th style="width:10%">
                                                                    الإجمالى
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="trItms">
                                                            <tr class="itms">
                                                                <td>
                                                                    <i class="fa fa-minus-circle text-danger delItm" idd="1" style=" cursor: pointer" ></i>
                                                                </td>
                                                                <td >
                                                                    <select class="pItm" idd="1" id="pItm_1" name="items[1][b_item]" style="height: 37px;width: 100%" >
                                                                        <option value="0"></option>
                                                                        <?php foreach ($items as $itm): ?>
                                                                            <option value="<?= $itm->itm_id ?>" style="color:red" ><?= $itm->itm_title ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>

                                                                </td>   
                                                                <td id="code_1"></td>   
                                                                <td><input type="text" idd="1" class="num" id="num_1" name="items[1][b_num]" style="width: 100%;height: 30px;border: 0;border-bottom: 1px dashed grey;background-color: transparent;cursor: text"/> </td>   
                                                                <td><input type="text" id="price_1" idd="1" class="price" name="items[1][b_price]" style="width: 100%;height: 30px;border: 0;border-bottom: 1px dashed grey;background-color: transparent;cursor: text"/> </td>   
                                                                <td class="bg-info" id="sum_1" style="color:white;font-weight: bold"> </td> 
                                                                 <input type="hidden" class="sum" idd="1" id="sumInput_1"  name="items[1][b_sum]" />
                                                            </tr>
                                                            <tr id="lastTR">
                                                                <td colspan="3" style="border:0" ></td>
                                                                <td id="totalNum" style="border:0;text-align: center"></td>
                                                                <td style="background: rgb(218, 223, 232)">إجمالى</td>
                                                                <td id="totalSum"></td>
                                                                 <input type="hidden" id="bils_total" name="bils_total" />
                                                            </tr>
                                                            <tr class="post" style="display: none">
                                                                <td colspan="4" style="border:0" ></td>
                                                                <td style="background: rgb(218, 223, 232)">نقدا</td>
                                                                <td><input  type="text" value="0" id="bils_cash" name="bils_cash" style="width: 100%;height: 30px;border: 0;border-bottom: 1px dashed grey;background-color: transparent;cursor: text"/></td>
                                                            </tr>
                                                            <tr class="post" style="display: none">
                                                                <td colspan="4" style="border:0" ></td>
                                                                <td style="background: rgb(218, 223, 232)">المطلوب</td>
                                                                <td id="bureSum">0</td>
                                                                <input type="hidden" id="bils_remain" name="bils_remain" />
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="">
                                                        <i title="إضافه عنصر للفاتوره" class="fa fa-plus-circle text-success" id="moreItms" style="cursor: pointer" ></i>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6 col-md-offset-3 margin-bottom-5" >
                                                    <input type='submit' class="btn btn-success btn-block" id="sub" name="sub" value="حفظ و طباعه" />
                                                    <input type='button' onclick="javascript:location.reload()" class="btn btn-info btn-block" style="display: none" id="nbill"  value="فاتوره جديدة" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- /Page body-->
                </div>
                <!--/ page content-->
                
            </div>
            <!-- /Page Container -->
            <!-- Main Container -->
        </div>
        <div id="recipt_print" style="display: none"></div>
        
        <!--Basic Scripts-->
        <script src="<?= THEME; ?>js/jquery-2.0.3.min.js"></script>
        <script src="<?= THEME; ?>js/bootstrap.min.js"></script>
        <script src="<?= THEME; ?>js/slimscroll/jquery.slimscroll.min.js"></script>

        <!--Beyond Scripts-->
        <script src="<?= THEME; ?>js/skins.min.js"></script>
        <script src="<?= THEME; ?>js/beyond.min.js"></script>
        <script src="<?= THEME; ?>js/toastr/toastr.js"></script>
        <script src="<?= THEME; ?>js/select2/select2.js"></script>
        <script src="<?= THEME; ?>js/jQuery.print.js"></script>

        <script>
            $(document).ready(function () {
                $("#bils_for").select2({
                    allowClear: true
                });

                $("#b_item").select2({
                    allowClear: true
                });

                $("#pItm_1").select2();
                
                
                $("#bils_type").change(function(){
                    var dt = $(this).val();
                    if(dt == 1){
                        $(".post").hide();
                        getSumPrice();
                    }else{
                        $(".post").show();
                        $("#bils_cash").val(0);
                        getSumPrice();
                      getRequiredPrice();
                    }
                });
                
                 $("#bils_for , #bils_type").change(function(){
                   checkBan();
                });
                
                function checkBan(){
                    var client = $("#bils_for").val();
                    var type = $("#bils_type").val();
                    if(client == 0){
                        $("#cl_case").text(" "); 
                        $("#sub").removeAttr('disabled');
                        return;
                    }
                    var curl = "<?= site_url('bills/isBan') ?>";
                    $.post(curl,{cl_id:client,ty:type},function(cl){
                       if(cl.done == 0){
                            $("#cl_case").text(cl.msg);
                            $("#sub").attr('disabled','disabled');
                       }else{
                        $("#cl_case").text(" "); 
                        $("#sub").removeAttr('disabled');
                      }
                    },'json');
                      }

                $(document).on('change', '.pItm', function () {
                    var itm_id = $(this).val();
                    var idd = $(this).attr('idd');
                    var purl = "<?= site_url('bills/items_prices') ?>/" + itm_id + '/' + idd;
                    $("#code_" + idd).load(purl, function () {
                        $("#xprice_" + idd).select2();
                    });

                });

                $(document).on('click', '#moreItms', function () {
                    var or = Date.now();
                    var itmUrl = "<?= site_url('bills/billItems') ?>/" + or;
                    $("#lastTR").before($('<tr class="itms">').load(itmUrl, function () {
                        $("#pItm_" + or).select2({allowClear: true});
                    }));
                });

                $(document).on('click', '.delItm', function () {
                    $(this).parents('tr').remove();
                    getCountItms();
                    getSumPrice();
                    getRequiredPrice();
                });
                
                $(document).on('keyup', '.price , .num', function () {
                    var idd = $(this).attr('idd');
                    var num = $('#num_'+idd).val();
                    var price = $('#price_'+idd).val();
                     if(isNaN(num) || num == ''){
                        num = 0;
                    }
                     if(isNaN(price) || price == ''){
                        price = 0;
                    }
                    var sum = parseFloat(num) * parseFloat(price);
                    $("#sum_"+idd).text(sum);
                    $("#sumInput_"+idd).val(sum);
                      getSumPrice();
                      getRequiredPrice();
                });
                
                $(document).on('keyup', '.num', function () {
                   getCountItms();
                   
                });
                
                $(document).on('keyup', '#bils_cash', function () {
                   getRequiredPrice(); 
                });
                
                //get count itms
                function getCountItms(){
                  var n = 0; 
                $('.num').each(function(){
                    var vn = $(this).val();
                    if(isNaN(vn) || vn == ''){
                        vn = 0; 
                        }
                   n +=parseFloat(vn);
                });      
                  $("#totalNum").text(n);  
                }
                
                //sum sum prices
                function getSumPrice(){
                    var s = 0; 
                $('.sum').each(function(){
                    var v = $(this).val();
                    if(isNaN(v) || v == ''){
                        v = 0;
                    }
                    s +=parseFloat(v);
                });      
                  $("#totalSum").text(s);
                  $("#bils_total").val(s);
                    }
                    
                //required price
                function getRequiredPrice(){
                var total = $("#bils_total").val(); 
                var cash = $("#bils_cash").val(); 
                var t = $("#bils_type").val(); 
                
                  if(isNaN(cash) || cash == ''){
                        cash = 0;
                    }
                
                 var remain = parseFloat(total) - parseFloat(cash);
                  $("#bureSum").text(remain);
                  $("#bils_remain").val(remain);
                    }
                    
               $("#bilFrm").submit(function(){
                   $("#sub").attr('disabled','disabled');
                   var data = $(this).serialize();
                   var url = "<?= site_url('bills/addBill') ?>";
                   $.post(url,data,function(r){
                       if(r.done == 0){
                      $("#billsError").html(r.msg);  
                       $("#sub").removeAttr('disabled');
                          }else{
                        $("#billsError").html(" ");
                           print_bublish(r.billId);
                           $("#sub").remove();
                           $("#nbill").show();
                           
                          }
                   },'json');
                   
                   return false;
               });
                   

            });
            
               function print_bublish(BillId){
                    var billUrl = "<?= site_url('bills/billDetails') ?>/" + BillId;
                    $("#recipt_print").load(billUrl, function () {
                        $.print("#bill");
                    });
                    
                   }  
        </script>
    </body>
    <!--  /Body -->
</html>
