<div class="col-lg-12 col-sm-12 col-xs-12" id="bill">
                            <div class="well invoice-container">
                                <div class="row">
                                  
                                    <div class="col-xs-6 ">
                                        <div >
                                            <h2 class="logo_font">مؤسسة سعد أبو قمر للتجارة</h2>
                                        </div>
                                 
                                        <div class="text-right">
                                              <h5 >010234567897 - 0123456789</h5>
                                            <h5 >21 ش المعز لدين الله الفاطمى</h5> 
                                        </div>
                                        <div class="horizontal-space"></div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="text-left"> <img src="<?= base_url("barcode.php?text=$bils_id")?>" alt="" style="width:200px; height:45px;" /> </div>
                                        <h5 class="text-left" style="direction: ltr;padding-left: 48px;">
                                            <!--<i class="fa fa-barcode"></i>-->
                                            <?=$bils_id?>
                                        </h5>
                                    </div>
                                </div>
                                <h2 class="logo_font" style=" opacity: 0.1;  -ms-transform: rotate(20deg); -webkit-transform: rotate(20deg); transform: rotate(20deg);">مؤسسة سعد أبو قمر للتجارة</h2>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h6 class="row recipt_font" >
                                                    <span class="col-xs-4"><?=$cl_name?></span>   
                                                    <span class="col-xs-3"> <?=date('d/m/Y',$created_on)?></span>   
                                                    <span class="col-xs-3"><?=date('H:i',$created_on)?></span>   
                                                    <span class="col-xs-2"> <?=($bils_type == 1)?'نقدى':'آجل'?></span>   
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                               
                                </div> <!-- / end client details section -->
                                 <h2 class="logo_font" style="text-align: left; opacity: 0.1;  -ms-transform: rotate(20deg); -webkit-transform: rotate(20deg); transform: rotate(20deg);">مؤسسة سعد أبو قمر للتجارة</h2>
                                <table class="table table-bordered table-striped recipt_font">
                                    <thead>
                                        <tr>
                                            <th><h5 class="no-margin-bottom">الصنف</h5></th>
                                            <th><h5 class="no-margin-bottom">السعر</h5></th>
                                            <th><h5 class="no-margin-bottom text-center">عدد</h5></th>
                                            <th><h5 class="no-margin-bottom text-center">اجمالى</h5></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                         $num = 0;
                                        foreach ($itms as $itm):
                                         $num += $itm['b_num'];    
                                          ?>
                                        <tr>
                                            <td ><?=$itm['itm_title']?></td>
                                            <td class="text-center"><?=$itm['b_price']?></td>
                                            <td class="text-center"><?=$itm['b_num']?></td>
                                            <td class="text-center"><?=$itm['b_sum']?></td>
                                        </tr>
                                         <?php endforeach;  ?>
                                       
                                        <tr>
                                            <td colspan="2" style="border:0"></td>
                                            <td class="text-center"><strong><?=$num?></strong></td>
                                            <td class="text-center "><strong><?=$bils_total?></strong></td>
                                        </tr>
                                        <?php if($bils_type == 2):  ?>
                                           <tr>
                                            <td colspan="2" style="border:0"></td>
                                            <td class="text-center"><strong>اجمالى</strong></td>
                                            <td class="text-center "><strong><?=$bils_total?></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="border:0"></td>
                                            <td class="text-center" style="background: green"><strong>نقدا</strong></td>
                                            <td class="text-center "><strong><?=$bils_cash?></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="border:0"></td>
                                            <td class="text-center"><strong>المطلوب </strong></td>
                                            <td class="text-center "><strong><?=$bils_remain?></strong></td>
                                        </tr>
                                        <?php endif;  ?>
                                    </tbody>
                                </table>
                                  <h2 class="logo_font" style="opacity: 0.1;  -ms-transform: rotate(20deg); -webkit-transform: rotate(20deg); transform: rotate(20deg);">مؤسسة سعد أبو قمر للتجارة</h2>
                                  <h2 class="logo_font" style="text-align: left; opacity: 0.1;  -ms-transform: rotate(20deg); -webkit-transform: rotate(20deg); transform: rotate(20deg);">مؤسسة سعد أبو قمر للتجارة</h2>
                                <div class="alert recipt_font" style="width: 250px;color: black;float: left;">
                                    <span>محاسب / </span> 
                                    <h5 class=""><?= $first_name ?></h5>
                                </div>
                                <p style="clear: both" ></p>
                            </div>
                        </div>