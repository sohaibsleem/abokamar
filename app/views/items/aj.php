        <div ><!-- modal -->
            <div class="modal fade" id="catAddModal" tabindex="-1" role="dialog" aria-labelledby="catAdd" style="z-index:9999">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="catAdd">اضافه تصنيف</h4>
                        </div>
                        <div class="modal-body">
                            <div id="frm_err" ></div>
                            <form id="catFrm" >
                                <div class="form-group">
                                    <label for="cat_title">الإسم</label>
                                    <input type="text" name="cat_title" class="form-control input-lg" id="cat_title">
                                </div>
                                <div class="form-group">
                                    <label for="cat_desc">ملاحظات</label>
                                    <input type="text" name="cat_desc" class="form-control input-xl" id="cat_desc">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="plzAddCat">اضافه</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">غلق</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div ><!-- modal -->
            <div class="modal fade" id="catModal" tabindex="-1" role="dialog" aria-labelledby="cat">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="cat">التصنيفات</h4>
                        </div>
                        <div class="modal-body">
<!--                                                               <div >
                                        <button class="btn btn-success btn-circle btn-xs" title="اضافه تصنيف" data-toggle="modal" data-target="#catAddModal" ><i class="fa fa-plus"></i></button>
                                   </div>-->
                          <h5 class="row-title">
                            <i class="typcn typcn-plus-outline"></i>
                            <a href="javascript:void(0)" class="btn btn-success btn-xs" title="اضافه تصنيف" data-toggle="modal" data-target="#catAddModal" >اضافه تصنيف</a> 
                        </h5>
                               <div class="col-xs-12 col-md-12">
                                <div class="well">
                                    <table class="table table-hover table-bordered">
                                        <thead class="bordered-blue">
                                            <tr>
                                                <th>
                                                    #
                                                </th>
                                                <th>
                                                    اسم التصنيف
                                                </th>
                                                <th>
                                                   ملاحظات
                                                </th>
                                                <th>
                                                    التحكم
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="cateTlist">
                                            <?php $this->load->view('items/cateTlist')  ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                       
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">غلق</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div ><!-- modal -->
    <div class="modal fade" id="pricModal" tabindex="-1" role="dialog" aria-labelledby="pric">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="pric">  <i class="fa fa-dollar" ></i> الأسعار المتعامل بها  <button class="btn btn-success btn-sm " id="apbtn">اضافه سعر جديد</button></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-hover table-bordered">
                        <thead class=" bordered-info">
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    كود السعر
                                </th>
                                <th>
                                    سعر الشراء
                                </th>
                                <th>
                                    سعر المخزن
                                </th>
                                <th>
                                    سعر الجمله
                                </th>
                                <th>
                                    الكميه المتاحه
                                </th>
                                <th>
                                   تاريخ الإنشاء
                                </th>
                                <th>
                                 تعطيل العمل بالسعر
                                </th>

                            </tr>
                        </thead>
                        <tbody id="itmPrices" >
                        
                            
                        </tbody>
                    </table>  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">غلق</button>

                </div>
            </div>
        </div>
    </div>
</div>

        <div ><!-- modal -->
            <div class="modal fade" id="pricAddModal" tabindex="-1" role="dialog" aria-labelledby="pricAdd" style="z-index:9999">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="pricAdd">اضافه كود سعر </h4>
                        </div>
                        <div class="modal-body">
                            <div id="price_frm_err" ></div>
                            <form id="pricFrm" >
                                <div class="form-group">
                                    <label for="pric_purchase"> سعر الشراء </label>
                                    <input type="text" name="pric_purchase" class="form-control" id="pric_purchase">
                                </div>
                                <div class="form-group">
                                    <label for="pric_stock">  سعر المخزن  </label>
                                    <input type="text" name="pric_stock" class="form-control" id="pric_stock">
                                </div>
                                <div class="form-group">
                                    <label for="pric_wholesale">   سعر الجمله  </label>
                                    <input type="text" name="pric_wholesale" class="form-control" id="pric_wholesale">
                                </div>
                                <div class="form-group">
                                    <label for="unit">الوحده</label>
                                    <input type="text" name="unit" class="form-control" id="unit">
                                </div>
                                <div class="form-group">
                                    <label for="stock">   الكميه المتاحه</label>
                                    <input type="text" name="stock" class="form-control" id="stock">
                                </div>
                                <div class="form-group">
                                    <label for="notes">ملاحظات</label>
                                    <input type="text" name="notes" class="form-control input-xl" id="notes">
                                </div>
                                <input name="itm_id" id="itm_id" type="hidden" />
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="plzAddCPric">اضافه</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">غلق</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>