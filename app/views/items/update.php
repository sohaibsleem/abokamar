<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('main/header'); ?>
    </head>
    <body class="main_font">
        <div class="navbar">
            <?php $this->load->view('main/upper_bar'); ?>  

        </div>
        <!-- Main Container -->
        <div class="main-container container-fluid">
            <!-- Page Container -->
            <div class="page-container">
                <!-- chat bar -->
                <?php $this->load->view('main/side_bar'); ?>    
                <!-- Page Content -->
                <div class="page-content">
                    <!-- Page Breadcrumb -->
                    <div class="page-breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= site_url('home') ?>">الرئيسيه</a>
                            </li>
                            <li>
                                <a href="<?= site_url('items') ?>">العناصر</a>
                            </li>
                            <li class="active">تعديل</li>
                            <li class="active"><?=$content->itm_title?></li>
                        </ul>
                    </div>
                    <!-- /Page Breadcrumb -->
                    <!-- Page Header -->
                    <div class="page-header position-relative">
                        <div class="header-title">
                            <h1>
                              تعديل بيانات العنصر
                            </h1>
                        </div>
                        <!--Header Buttons-->
                        <div class="header-buttons">
                            <a class="sidebar-toggler" href="#">
                                <i class="fa fa-arrows-h"></i>
                            </a>
                            <a class="refresh" id="refresh-toggler" href="">
                                <i class="glyphicon glyphicon-refresh"></i>
                            </a>
                            <a class="fullscreen" id="fullscreen-toggler" href="#">
                                <i class="glyphicon glyphicon-fullscreen"></i>
                            </a>
                        </div>
                        <!--Header Buttons End-->
                    </div>

                 <div class="page-body">
                        <h5 class="row-title"><i class="typcn typcn-th-list"></i><a href="<?= site_url('items/all')?>" >العناصر</a></h5>
                        <div class="row">
                            
                            <div class="col-lg-9 col-md-offset-1 col-sm-12 col-xs-12">
                                 <?= $this->session->flashdata('itm_Err');   ?>
                                    <div class="widget">
                                        <div class="widget-header bordered-bottom bordered-themeprimary">
                                            <span class="widget-caption"><i class="typcn typcn-pencil"></i> <?=$content->itm_title?></span>
                                          </div>
                                        <div class="widget-body bordered-left bordered-themeprimary">
                                            <div class="collapse in">
                                                <form role="form" action="<?= site_url("items/update/$content->itm_id")?>" method="POST">
                                                  
                                                    <div class="form-group">
                                                        <label for="itm_title">اسم العنصر <span class="text-danger">*</span> </label>
                                                        <input type="text" class="form-control" value="<?= $content->itm_title; ?>" name="itm_title" id="itm_title"  >
                                                        <?= form_error('itm_title', '<p class="text-danger">', '</p>'); ?>
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="itm_desc">الوصف  </label>
                                                        <textarea id="itm_desc" class="form-control" name="itm_desc"><?= $content->itm_desc; ?></textarea>
                                                    </div>
                                                      <div class="form-group">
                                                            <label for="itm_cat">التصنيف <span class="text-danger">*</span></label>
                                                            <select id="itm_cat"  name="itm_cat" style="height: 37px;width: 100%" >
                                                                <option value="0">اختر تصنيف</option>
                                                                  <?php foreach($categories as $cat):   ?>
                                                                <option value="<?=$cat->cat_id?>" <?=($content->itm_cat == $cat->cat_id )?'selected':''?>><?=$cat->cat_title?></option>
                                                                      <?php endforeach;  ?>
                                                            </select>
                                                            <?= form_error('itm_cat', '<p class="text-danger">', '</p>'); ?>
                                                        </div>
                                                    <div class="form-group">
                                                        <label for="itm_barcode_title">العنوان على الباركود <span class="text-danger">*</span> </label>
                                                        <input type="text" class="form-control" value="<?= $content->itm_barcode_title; ?>" name="itm_barcode_title" id="itm_barcode_title" >
                                                        <?= form_error('itm_barcode_title', '<p class="text-danger">', '</p>'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="itm_barcode_price">السعر على الباركود <span class="text-danger">*</span> </label>
                                                        <input type="text" value="<?= $content->itm_barcode_price; ?>" class="form-control" name="itm_barcode_price" id="itm_barcode_price" >
                                                        <?= form_error('itm_barcode_price', '<p class="text-danger">', '</p>'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="itm_primary_price">سعر الشراء <span class="text-danger">*</span> </label>
                                                        <input type="text" value="<?= $content->itm_primary_price; ?>" class="form-control" name="itm_primary_price" id="itm_primary_price" >
                                                        <?= form_error('itm_primary_price', '<p class="text-danger">', '</p>'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="itm_stock_price">سعر المخزن <span class="text-danger">*</span> </label>
                                                        <input type="text" value="<?= $content->itm_stock_price; ?>" class="form-control" name="itm_stock_price" id="itm_stock_price" >
                                                        <?= form_error('itm_stock_price', '<p class="text-danger">', '</p>'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="itm_wholesale_price">سعر الجملة <span class="text-danger">*</span> </label>
                                                        <input type="text" value="<?= $content->itm_wholesale_price; ?>" class="form-control" name="itm_wholesale_price" id="itm_wholesale_price" >
                                                        <?= form_error('itm_wholesale_price', '<p class="text-danger">', '</p>'); ?>
                                                    </div>
                                                  
                                                    <div class="form-group col-md-6 col-md-offset-3 margin-bottom-5" >
                                                        <input type='submit' class="btn btn-success btn-block" name="sub" value="تعديل بيانات العنصر" />
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>

                </div>  <!-- /Page body-->
                </div>
                <!--/ page content-->

            </div>
            <!-- /Page Container -->
            <!-- Main Container -->
        </div>
        <!--Basic Scripts-->
        <script src="<?= THEME; ?>js/jquery-2.0.3.min.js"></script>
        <script src="<?= THEME; ?>js/bootstrap.min.js"></script>
        <script src="<?= THEME; ?>js/slimscroll/jquery.slimscroll.min.js"></script>

        <!--Beyond Scripts-->
        <script src="<?= THEME; ?>js/skins.min.js"></script>
        <script src="<?= THEME; ?>js/beyond.min.js"></script>
        <script src="<?= THEME; ?>js/toastr/toastr.js"></script>
        <script src="<?= THEME; ?>js/select2/select2.js"></script>
        
        <script>
        $(document).ready(function(){
           $("#itm_cat").select2({
            placeholder: "Select a State",
            allowClear: true
        });  
            
        });
        </script>
    </body>
    <!--  /Body -->
</html>
