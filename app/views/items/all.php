<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('main/header'); ?>
    </head>
    <body class="main_font">
        <div class="navbar">
            <?php $this->load->view('main/upper_bar'); ?>  

        </div>
        <!-- Main Container -->
        <div class="main-container container-fluid">
            <!-- Page Container -->
            <div class="page-container">
                <!-- chat bar -->
                <?php $this->load->view('main/side_bar'); ?>    
                <!-- Page Content -->
                <div class="page-content">
                    <!-- Page Breadcrumb -->
                    <div class="page-breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= site_url('home') ?>">الرئيسيه</a>
                            </li>
                            <li class="active">الأصناف</li>
                        </ul>
                    </div>
                    <!-- /Page Breadcrumb -->
                    <!-- Page Header -->
                    <div class="page-header position-relative">
                        <div class="header-title">
                            <h1>
                                الأصناف
                            </h1>
                        </div>
                        <!--Header Buttons-->
                        <div class="header-buttons">
                            <a class="sidebar-toggler" href="#">
                                <i class="fa fa-arrows-h"></i>
                            </a>
                            <a class="refresh" id="refresh-toggler" href="">
                                <i class="glyphicon glyphicon-refresh"></i>
                            </a>
                            <a class="fullscreen" id="fullscreen-toggler" href="#">
                                <i class="glyphicon glyphicon-fullscreen"></i>
                            </a>
                        </div>
                        <!--Header Buttons End-->
                    </div>

                    <div class="page-body">
                        <h5 class="row-title" style="margin-left:10px">
                            <i class="typcn typcn-th-list-outline"></i>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#catModal">التصنيفات</a> 
                        </h5> 
                        <h5 class="row-title">
                            <i class="typcn typcn-plus-outline"></i>
                            <a href="<?= site_url("items/add") ?>" >اضافه عنصر جديد</a> 
                        </h5> 
                        
                       
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="well with-header  with-footer">
                                    <div class="header bg-info">
                                        العناصر
                                    </div>
                                    <table class="table table-hover table-bordered">
                                        <thead class=" bordered-info">
                                            <tr>
                                                <th>
                                                    #
                                                </th>
                                                <th>
                                                    اسم العنصر
                                                </th>
                                                <th>
                                                   تصنيف
                                                </th>
                                                <th>
                                                    تاريخ الاضافه   
                                                    /
                                                    بواسطه
                                                </th>
                                                <th>
                                                    اخر التعديل   
                                                    /
                                                    بواسطه
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($items as $k => $itm): ?>
                                                <tr id="tr_<?= $itm->itm_id ?>">
                                                    <td>
                                                        <?= $k + 1 ?>
                                                        <div class="btn-group">
                                                            <a title="الاعدادات" data-toggle="dropdown">
                                                                <i class="glyphicon glyphicon-cog"></i>
                                                            </a>
                                                            <ul class="dropdown-menu">
                                                                <li>
                                                                    <a target="_blank" href=""><i class="fa fa-eye"></i> عرض البيانات</a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li>
                                                                    <a  href="javascript:void(0)" class="pric" idd="<?= $itm->itm_id ?>" ><i class="fa fa-dollar"></i> الأسعار</a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li>
                                                                    <a href="<?= site_url("items/update/$itm->itm_id") ?>"><i class="fa fa-pencil"></i> تعديل</a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li>
                                                                    <a href="javascript:void(0);" class="delIitm" idd="<?= $itm->itm_id ?>"><i class="fa fa-trash-o"></i>حذف </a>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?= $itm-> itm_title  ?>                                     
                                                    </td>
                                                    <td>
                                                        <?= $itm->cat_title ?>                                     
                                                    </td>
                                                    <td>
                                                        <?= date('d/m/Y H:i:s', $itm->created_on) ?> <br/>
                                                        <?= $this->ion_auth->user($itm->created_by)->row()->first_name; ?> 

                                                    </td>
                                                    <td>
                                                        <?= ($itm->modified_on) ? date('d/m/Y H:i:s', $itm->modified_on) : '' ?> <br/>
                                                        <?= ($itm->modified_by) ? $this->ion_auth->user($itm->modified_by)->row()->first_name : '' ?> 
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                    <div class="footer">
                                        <?= $this->pagination->create_links(); ?>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>  <!-- /Page body-->
                </div>
                <!--/ page content-->

            </div>
            <!-- /Page Container -->
            <!-- Main Container -->
        </div>
        <?php $this->load->view('items/aj');   ?>

        
        
        <!--Basic Scripts-->
        <script src="<?= THEME; ?>js/jquery-2.0.3.min.js"></script>
        <script src="<?= THEME; ?>js/bootstrap.min.js"></script>
        <script src="<?= THEME; ?>js/slimscroll/jquery.slimscroll.min.js"></script>

        <!--Beyond Scripts-->
        <script src="<?= THEME; ?>js/skins.min.js"></script>
        <script src="<?= THEME; ?>js/beyond.min.js"></script>
        <script src="<?= THEME; ?>js/toastr/toastr.js"></script>
        <script>
            jQuery(document).ready(function () {
                //add category
                $("#plzAddCat").click(function () {
                    var data = $("#catFrm").serialize();
                    var url = "<?= site_url('items/addCat') ?>";
                    $.post(url, data, function (res) {
                        if (res.done == 1) {
                         $("#cateTlist").load("<?= site_url('items/updateTable')?>");
                          $("#catAddModal").modal("hide");
                        } else {
                            $("#frm_err").html(res.msg);
                        }

                    }, 'json');


                });


                $("#catAddModal").on('hidden.bs.modal', function (e) {
                    $("#catFrm")[0].reset();
                    $("#frm_err").html(' ');

                });

                //edit category
                   $(document).on('click','.editCat',function () {
                    var th = $(this);
                    var idd = th.data('id');
                    var cur_title = th.data('cur_title');
                    var cat_title = $('#catData_title_' + idd).val();
                    var cat_desc = $('#catData_desc_' + idd).val();
                    var url = "<?= site_url('items/edit_cat') ?>";
                    $.post(url, {id: idd,cur_title:cur_title, cat_title: cat_title, cat_desc: cat_desc}, function (res) {
                        if (res.done == 1) {
                            Notify(res.msg, 'top-right', '5000', 'success', 'fa-check', true);

                        } else {
                            Notify(res.msg, 'top-right', '10000', 'danger', 'fa-times', true);
                        }
                    }, 'json');
                });


                //delete category
                $(document).on('click','.delCat',function () {
                    var conf = confirm('هل ترغب فى حذف التصنيف؟');
                    if (!conf) {
                        return false;
                    }
                    var th = $(this);
                    var idd = th.data('id');
                    var url = "<?= site_url('items/delete_cat') ?>";
                    $.post(url, {id: idd}, function (res) {
                        if (res.done == 1) {
                            $('#trCat_' + idd).remove();
                            Notify(res.msg, 'top-right', '5000', 'success', 'fa-check', true);

                        } else {
                            Notify(res.msg, 'top-right', '10000', 'danger', 'fa-times', true);
                        }
                    }, 'json');
                });

                $('.delIitm').click(function () {
                    var conf = confirm('من فضلك قم بتأكيد حذف العنصر');
                    if (!conf) {
                        return false;
                    }
                    var th = $(this);
                    var idd = th.attr('idd');
                    var url = "<?= site_url('items/delete') ?>";
                    $.post(url, {id: idd}, function (res) {
                        if (res.done == 1) {
                            $('#tr_' + idd).remove();
                            Notify(res.msg, 'top-right', '5000', 'success', 'fa-check', true);

                        } else {
                            Notify(res.msg, 'top-right', '10000', 'danger', 'fa-times', true);
                        }
                    }, 'json');
                });
                
                
                //prices
                $(".pric").click(function(){
                    var id = $(this).attr('idd');
                    var purl = "<?= site_url('items/getItemPrices') ?>/"+id;
                    $("#apbtn").attr("idd",id);
                    $("#itmPrices").load(purl);
                    $("#pricModal").modal();
                    
                });
                
                $("#pricModal").on('hidden.bs.modal', function (e) {
                    $("#apbtn").removeAttr("idd");
                    $("#itmPrices").html(" ");

                });
                
                
                $("#apbtn").click(function(){
                  var id = $(this).attr('idd');
                  $("#itm_id").val(id);
                  $("#pricAddModal").modal();
                  
                });
                
                   //add price
                $("#plzAddCPric").click(function () {
                    var data = $("#pricFrm").serialize();
                    var url = "<?= site_url('items/addPrice') ?>";
                    $.post(url, data, function (res) {
                        if (res.done == 1) {
                           var id = $("#apbtn").attr('idd');
                           var purl = "<?= site_url('items/getItemPrices') ?>/"+id;
                          $("#itmPrices").load(purl);
                          $("#pricAddModal").modal("hide");
                           Notify("تم اضافه السعر", 'top-right', '5000', 'success', 'fa-check', true);
                        } else {
                            $("#price_frm_err").html(res.msg);
                        }

                    }, 'json');


                });
                
                $("#pricAddModal").on('hidden.bs.modal', function (e) {
                    $("#pricFrm")[0].reset();
                    $("#price_frm_err").html(' ');

                });
            });

        </script>
    </body>
    <!--  /Body -->
</html>
