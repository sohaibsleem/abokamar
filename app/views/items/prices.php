<?php foreach ($prices as $k => $pric): ?>
    <tr id="tr_<?= $pric->pric_id ?>">
        <td>
            <?= $k + 1 ?>
        </td>
        <td>
            <span class=" badge badge-green" >  <?= $pric->pric_id ?>      </span>                               
        </td>
        <td>
            <?= $pric->pric_purchase ?>                                     
        </td>
        <td>
            <?= $pric->pric_stock ?>                                     
        </td>
        <td>
            <?= $pric->pric_wholesale ?>                                     
        </td>
        <td>
            <?= $pric->stock ?>                                     
        </td>
        <td>
            <?= date('d/m/Y H:i:s', $pric->created_on) ?> <br/>

        </td>
        <td>
            

        </td>

    </tr>
<?php endforeach; ?> 