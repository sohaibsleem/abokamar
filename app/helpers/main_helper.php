<?php

function error_msg($msg) {
    $err = '<div class="alert alert-danger">';
    $err .= '<button data-dismiss="alert" class="close">×</button>';
    $err .= '<i class="fa fa-times-circle"></i> <strong>خطأ </strong> ';
    $err .= $msg;
    $err .= '</div>';
    return $err;
}

function success_msg($msg) {
    $err = '<div class="alert alert-success">';
    $err .= '<button data-dismiss="alert" class="close">×</button>';
    $err .= '<i class="fa fa-ok"></i> <strong>تم! </strong> ';
    $err .= $msg;
    $err .= '</div>';
    return $err;
}

function adminType($type){
    $data = [
        '2'=>'عن المدرسه',
        '3'=>'الرؤيه و الرساله',
        '4'=>'الرساله الترحيبيه',
        '5'=>'كلمه مدير المدرسه'
    ];
    return $data[$type];
}

function getCategory($c){
    $ci = &get_instance();
    return $ci->db->where('cate_id',$c)->get('activities_category')->row();
}

function countVisitorMessage(){
    $v = &get_instance();
    return $v->db->get_where('vistors_msg',['seen'=>0])->num_rows();
}
function countStudentaApply(){
    $s = &get_instance();
    return $s->db->get_where('applystudent',['seen'=>0])->num_rows();
}

function countCareerspply(){
    $c = &get_instance();
    return $c->db->get_where('careers_apply',['seen'=>0])->num_rows();
}

function visitors(){
    $vi =&get_instance();
     return $vi->db->get('visitors')->num_rows();
}

function defaultLang(){
    $lan =&get_instance();
     return $lan->db->select('set_lang')->get('settings')->row();
}