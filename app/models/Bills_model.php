<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bills_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

   function getClients(){
       $this->db->order_by('cl_name');
       return $this->db->get('clients')->result();
   }
   
   function getItems(){
     $this->db->order_by('itm_title');
     $this->db->where('active',1);  
     return $this->db->get('items')->result();
   }
   
   function getItmPrices($itm){
       return $this->db->where(['itm_id'=>$itm,'active'=>1])->get('item_prices')->result();
   }
   
   function is_ban(){
       $this->db->select('cl_ban,cl_trans_type');
       $this->db->where('cl_id',$this->input->post('cl_id'));
       return $this->db->get('clients')->row();
   }

           
   function addBill() {
        $data = [
            'bils_for' => $this->input->post('bils_for'),
            'bils_type' => $this->input->post('bils_type'),
            'bils_total' => $this->input->post('bils_total'),
            'bils_cash' => $this->input->post('bils_cash'),
            'bils_remain' => $this->input->post('bils_remain'),
            'created_by' => $this->user,
            'created_on' =>  time()
        ];
        if($this->db->insert('bills', $data)){
       $lastID = $this->db->insert_id();
       $this->addBillItems($lastID , $this->input->post('items'));
       if($this->input->post('bils_type') == 2){
           $this->add_bill_transaction($data['bils_for'], $data['bils_remain'], $lastID);
       }
       return $lastID;
        }
        return FALSE;
       
    }
    
    function addBillItems($lastID, $itms) {
        $data = [];
        foreach ($itms as $k => $itm) {
            $data[$k]['bill_id'] = $lastID;
            $data[$k]['b_item'] = $itm['b_item'];
            $data[$k]['b_price_code'] = $itm['b_price_code'];
            $data[$k]['b_price'] = $itm['b_price'];
            $data[$k]['b_num'] = $itm['b_num'];
            $data[$k]['b_sum'] = $itm['b_sum'];
        }

        return $this->db->insert_batch('bill_items', $data);
    }
    
     function add_bill_transaction($client,$debit,$bill){
         $data = [
             'tran_type'=>1,
             'tran_client'=>$client,
             'tran_debit'=>$debit,
             'tran_bill'=>$bill,
             'tran_date'=>time()
         ];
         return $this->db->insert('client_transactions',$data);
     }

    function getBillDetails($id){
       $this->db->select("bills.*,bill_items.*,items.itm_title,users.first_name,clients.cl_name");
       $this->db->from('bills');
       $this->db->join('bill_items','bill_items.bill_id=bills.bils_id');
       $this->db->join('items','items.itm_id=bill_items.b_item');
       $this->db->join('clients','clients.cl_id=bills.bils_for');
       $this->db->join('users','users.id=bills.created_by');
       $this->db->where('bills.bils_id',$id);
       return $this->db->get()->result();
    }
    
    
    function update() {
     $data = [
            'cl_name' => $this->input->post('cl_name'),
            'cl_mobile' => $this->input->post('cl_mobile'),
            'cl_phone' => $this->input->post('cl_phone'),
            'cl_info' => $this->input->post('cl_info'),
            'cl_trans_type' => $this->input->post('cl_trans_type'),
            'cl_ban' => ($this->input->post('cl_ban'))?1:0,
            'cl_ban_resone' => $this->input->post('cl_ban_resone'),
            'created_on' =>  time()
        ];
        $this->db->where('cl_id',$this->input->post('cl_id'));
        return $this->db->update('clients', $data);
    }
    
    
    function getDetails($id){
        $this->db->where('cl_id',$id);
        return $this->db->get('clients')->row();
    }
    
     function deleteCli() {
        $this->db->where('cl_id',$this->input->post('id'));
        return $this->db->delete('clients');
    }
    
 
      function checkValidDeleteClient() {
        return (bool) $this->db->get_where('clients', array('cl_id' => $this->input->post('id')))->num_rows();
    }

}
