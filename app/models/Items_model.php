<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

   function getItems($limit, $offset){
       $this->db->select('items.*,items_category.cat_title');
       $this->db->join('items_category','items_category.cat_id=items.itm_cat');
       $this->db->order_by('items.itm_id','desc');
       $this->db->limit($limit,$offset);
       return $this->db->get('items')->result();
   }
   
   function addItems() {
        $data = [
            'itm_cat' => $this->input->post('itm_cat'),
            'itm_title' => $this->input->post('itm_title'),
            'itm_desc' => $this->input->post('itm_desc'),
            'created_on' => time(),
            'created_by' => $this->user
        ];
        return $this->db->insert('items', $data);
    }
    
    
   function updateItems($id) {
        $data = [
            'itm_cat' => $this->input->post('itm_cat'),
            'itm_title' => $this->input->post('itm_title'),
            'itm_desc' => $this->input->post('itm_desc'),
            'modified_on' => time(),
            'modified_by' => $this->user
        ];
        $this->db->where('itm_id',$id);
        return $this->db->update('items', $data);
    }
    
    
    function getDetails($id){
        $this->db->where('itm_id',$id);
        return $this->db->get('items')->row();
    }
    
     function deleteItm() {
        $id = $this->input->post('id');
        $this->db->where('itm_id',$id);
        return $this->db->delete('items');
    }
    
    
    
    #categories________________________________________________________________________
    function getCategory() {
        return $this->db->get('items_category')->result();
    }

    function addCat() {
        $data = [
            'cat_title' => $this->input->post('cat_title'),
            'cat_desc' => $this->input->post('cat_desc'),
                
                ];

        return  $this->db->insert('items_category', $data);
    }

    function deleteCat() {
        $cat_id = $this->input->post('id');
        $this->db->where('cat_id', $cat_id);
        return $this->db->delete('items_category');
    }


      function editCat() {
        $data = [
            'cat_title' => $this->input->post('cat_title'),
            'cat_desc' => $this->input->post('cat_desc'),
                
                ];
        $this->db->set($data);
        $this->db->where('cat_id', $this->input->post('id'));
        return $this->db->update('items_category');
    }
      function checkValidDeleteCat() {
        $cat_id = $this->input->post('id');
        return (bool) $this->db->get_where('items', array('itm_cat' => $cat_id))->num_rows();
    }
    
   //prices 
    
    function getItemPrices($id){ 
    return $this->db->where('itm_id',$id)->get('item_prices')->result();    
    }
    
    
    function addPrice(){
       $data = [
            'itm_id' => $this->input->post('itm_id'),
            'pric_purchase' => $this->input->post('pric_purchase'),
            'pric_stock' => $this->input->post('pric_stock'),
            'pric_wholesale' => $this->input->post('pric_wholesale'),
            'stock' => $this->input->post('stock'),
            'unit' => $this->input->post('unit'),
            'active' => 1,
            'created_on' => time(),
            'unit' => $this->input->post('unit')    
                ];

        return  $this->db->insert('item_prices', $data);  
    }

}
