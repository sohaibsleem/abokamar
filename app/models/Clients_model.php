<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

   function getClients($limit, $offset){
       $this->db->order_by('cl_name');
       $this->db->limit($limit,$offset);
       return $this->db->get('clients')->result();
   }
   
   function add() {
        $data = [
            'cl_name' => $this->input->post('cl_name'),
            'cl_mobile' => $this->input->post('cl_mobile'),
            'cl_phone' => $this->input->post('cl_phone'),
            'cl_info' => $this->input->post('cl_info'),
            'cl_trans_type' => $this->input->post('cl_trans_type'),
            'cl_ban' => ($this->input->post('cl_ban'))?1:0,
            'cl_ban_resone' => $this->input->post('cl_ban_resone'),
            'created_on' =>  time()
        ];
        return $this->db->insert('clients', $data);
    }
    
    
   function update() {
     $data = [
            'cl_name' => $this->input->post('cl_name'),
            'cl_mobile' => $this->input->post('cl_mobile'),
            'cl_phone' => $this->input->post('cl_phone'),
            'cl_info' => $this->input->post('cl_info'),
            'cl_trans_type' => $this->input->post('cl_trans_type'),
            'cl_ban' => ($this->input->post('cl_ban'))?1:0,
            'cl_ban_resone' => $this->input->post('cl_ban_resone'),
            'created_on' =>  time()
        ];
        $this->db->where('cl_id',$this->input->post('cl_id'));
        return $this->db->update('clients', $data);
    }
    
    
    function getDetails($id){
        $this->db->where('cl_id',$id);
        return $this->db->get('clients')->row();
    }
    
     function deleteCli() {
        $this->db->where('cl_id',$this->input->post('id'));
        return $this->db->delete('clients');
    }
    
 
      function checkValidDeleteClient() {
        return (bool) $this->db->get_where('clients', array('cl_id' => $this->input->post('id')))->num_rows();
    }

}
